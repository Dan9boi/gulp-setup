"use strict";

/*
Gulp installation:
-------------------
cd c:\"Visual studio projects"\project-name\Project.Website
npm install

Start Gulp
-------------------
cd c:\"Visual studio projects"\project-name\Project.Website
gulp watch // for starting listening for file changes
gulp build // for building all resources

Stop / restart Gulp
-------------------
ctrl + c
run gulp command to start gulp again


*/

/*!
 * gulp
 * npm install gulp-less gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev
 */

// Load plugins
var gulp = require('gulp'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del'),
    plumber = require('gulp-plumber');

// Load config file
//var config = require('gulpconfig.json');

// Setup error handler
var onError = function (err) {  
  notify.onError({
      title:    "Gulp",
      subtitle: "Failure!",
      message:  "Error: <%= error.message %>",
      sound:    "Beep"
  })(err);

  this.emit('end');
};

// Styles
gulp.task('styles', function() {
  return gulp.src('less/master.less')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(less())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(minifycss())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('css/compiled'))
    .pipe(notify({ message: 'Styles task complete' }));
});

// Scripts
gulp.task('scripts', function() {
  return gulp.src('scripts/**/*.js')
    .pipe(plumber({
      errorHandler: onError
    }))
    //.pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter(stylish))
    .pipe(concat('master.js'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('scripts/compiled'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

// Images
gulp.task('images', function() {
  return gulp.src('images/**/*')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('images'))
    .pipe(notify({ message: 'Images task complete' }));
});

// Clean
gulp.task('clean', function(cb) {
    del(['css/compiled', 'js/compiled'], cb)
});

// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts', 'images');
});

// Watch
gulp.task('watch', function() {

  // Watch .less files
  gulp.watch('less/**/*.less', ['styles']);

  // Watch .js files
  gulp.watch('scripts/**/*.js', ['scripts']);

  // Watch image files
  gulp.watch('images/**/*', ['images']);

  // Create LiveReload server
  livereload.listen();

  // Watch any compiled files, reload on change
  gulp.watch(['css/compiled/*.css', 'scripts/compiled/*.js']).on('change', livereload.changed);

});