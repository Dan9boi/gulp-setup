/**
 * Build all assets
 */

// Dependencies
var gulp = require('gulp');
var rimraf = require('gulp-rimraf');
var config = require('../config');

gulp.task('clean', function() {
    return gulp.src([config.style.dist, config.browserify.dist], {
            read: false
        })
        .pipe(rimraf({
            force: true
        }));
});

/**
 * Export Gulp job
 * @return        {object}        Gulp stream
 */
gulp.task('build', ['styles', 'scripts', 'icons']);

gulp.task('rebuild', ['clean', 'zip-node'], function() {
    gulp.start('build');
});
