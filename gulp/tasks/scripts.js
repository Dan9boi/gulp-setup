/**
 * Bundle javascript with Browserify, minify and make source maps.
 * The bundeled files are split into custom scripts and a file with vendor scripts.
 */

// Dependencies
var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var path = require('path');
var rev = require('gulp-rev');
var buffer = require('gulp-buffer');
var hbsfy = require('hbsfy');
var config = require('../config');
var rename = require('gulp-rename');

var bundle = function(browserifyStream, name) {
    var stream = browserifyStream
        .transform(hbsfy)
        .plugin('minifyify', {
            map: name + '.js.map',
            output: config.browserify.dist + '/' + name + '.js.map',
            compressPath: function(p) {
                return path.relative('./' + config.browserify.dist, p).replace(/\\/g, '/');
            }
        })
        .bundle()
        .pipe(source(name + '.js'))
        .pipe(buffer());

    stream
        .pipe(gulp.dest(config.browserify.dist))
        .pipe(rev())
        .pipe(gulp.dest(config.browserify.dist))
        .pipe(rev.manifest())
        .pipe(rename({
            basename: name,
            suffix: '.js.rev'
        }))
        .pipe(gulp.dest(config.browserify.dist));

    return stream;
};

gulp.task('scripts', function() {

    config.browserify.entries.forEach(function(entry) {
        var masterStream = browserify({
            entries: './' + config.browserify.baseDir + '/' + entry + '.js',
            debug: true
        });
        bundle(masterStream, entry)
            .pipe(gulp.dest(config.browserify.dist));
    });

});
