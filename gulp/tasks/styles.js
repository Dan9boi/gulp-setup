/**
 * Compile less files into a css file using autoprefix for CSS3 prefixes.
 * Inlcude source maps for debugging
 */

// Dependencies
var gulp = require('gulp');
var less = require('gulp-less');
var sourcemaps = require('gulp-sourcemaps');
var prefix = require('gulp-autoprefixer');
var fs = require('fs');
var rev = require('gulp-rev');
var config = require('../config');
var rename = require('gulp-rename');
var gulpFilter = require('gulp-filter');

gulp.task('compile-less', function(done) {

    var count = 0;

    config.style.entries.forEach(function(entry) {

        var cssFilter = gulpFilter('*.css');

        gulp.src(config.style.baseDir + '/' + entry + '.less')
            .pipe(sourcemaps.init())
            .pipe(less({
                compress: true
            }))
            .pipe(sourcemaps.write())
            .pipe(prefix({
                map: true
            }))
            .pipe(sourcemaps.init({
                loadMaps: true
            }))
            .pipe(sourcemaps.write('.', {
                sourceRoot: '/' + config.style.baseDir + '/'
            }))
            .pipe(gulp.dest(config.style.dist))
            .pipe(cssFilter)
            .pipe(rev())
            .pipe(gulp.dest(config.style.dist))
            .pipe(rev.manifest())
            .pipe(rename({
                basename: entry,
                suffix: '.css.rev'
            }))
            .pipe(gulp.dest(config.style.dist))
            .on('end', function() {
                count++;
                if (count === config.style.entries.length) {
                    done();
                }
            });
    });
});

gulp.task('css-sourcemaps-path-rewrite', function() {
    config.style.entries.forEach(function(entry) {
        var sourceMapPath = './' + config.style.dist + '/' + entry + '.css.map';
        var sourceMap = JSON.parse(fs.readFileSync(sourceMapPath));
        for (var i = 0; i < sourceMap.sources.length; i++) {
            sourceMap.sources[i] = sourceMap.sources[i].replace(/\\/g, '/');
        }
        fs.writeFileSync(sourceMapPath, JSON.stringify(sourceMap));
    });
});

gulp.task('styles', ['compile-less'], function() {
    gulp.start('css-sourcemaps-path-rewrite');
});
